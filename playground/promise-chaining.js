require('../src/db/mongoose')
const User = require('../src/models/user')

// User.findByIdAndUpdate('5dd53ba5a9e8d51d907718cd', { age: 1 }).then((user) => {
//     console.log(user)
//     return User.countDocuments({ age: 1 })
// }).then((result) => {
//     console.log(result)
// }).catch((e) => {
//     console.log(e)
// })


const updateAgeAndCount = async (id, age    ) => {
    const user = await User.findByIdAndUpdate(id, { age })
    const count = await User.countDocuments({ age })
    return count
}

updateAgeAndCount('5dd53bdbe63fb44e381ac37e', 2).then((count) => {
    console.log('count: ', count)
}).catch((e) => {
    console.log('e: ', e)
})