const express = require('express')
require('./db/mongoose')
const userRouter = require('./routers/user')
const taskRouter = require('./routers/task')

const app = express()
const port = process.env.PORT || 3000

app.use(express.json())                         //parsing JSON coming in request body
app.use(userRouter)                             //Registering router with app
app.use(taskRouter)  

app.listen(port, () => {
    console.log('Server is up on port', + port)
})
